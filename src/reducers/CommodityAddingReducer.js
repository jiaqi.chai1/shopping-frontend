const initStates = {
  name: null,
  eachPrice: 0,
  measureWord: null,
  imageUrl: null
};

const backUp = initStates;

export default (states = initStates, action) => {
  switch (action.type) {
    case 'STASH_NAME':
      return {
        ...states,
        name: action.name
      };
    case 'STASH_EACH_PRICE':
      return {
        ...states,
        eachPrice: action.eachPrice
      };
    case 'STASH_MEASURE_WORD':
      return {
        ...states,
        measureWord: action.measureWord
      };
    case 'STASH_IMAGE_URL':
      return {
        ...states,
        imageUrl: action.imageUrl
      };
    case 'ADD_COMMODITY':
      return {
        ...backUp
      };
    default:
      return states;
  }
}