const initStates = {
  orders: []
};

export default (states = initStates, action) => {
  switch (action.type) {
    case 'GET_ORDERS':
      return {
        ...states,
        orders: action.orders
      };
    default:
      return states;
  }
}