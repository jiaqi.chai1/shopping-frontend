import {combineReducers} from "redux";
import HomeReducer from "./HomeReducer";
import CommodityAddingReducer from "./CommodityAddingReducer";
import CommodityOrdersReducer from "./CommodityOrdersReducer";

const reducers = combineReducers({
  home: HomeReducer,
  adding: CommodityAddingReducer,
  order: CommodityOrdersReducer
});
export default reducers;