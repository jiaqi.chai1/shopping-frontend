const initStates = {
  commodities: []
};

export default (states = initStates, action) => {
  switch (action.type) {
    case 'GET_COMMODITIES':
      return {
        ...states,
        commodities: action.commodities
      };
    default:
      return states;
  }
}