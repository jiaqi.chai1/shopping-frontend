import React, {Component} from 'react';

class Order extends Component {
  render() {
    const order = this.props.order;
    return (
      <div className={'Order'}>
        <div>
          <span>{order.commodityName}</span>
          <span>{order.eachPrice}</span>
          <span>{order.quantity}</span>
          <span>{order.measureWord}</span>
          <button onClick={this.deleteOrder.bind(this)}>删除</button>
        </div>
        <hr/>
      </div>
    );
  }

  deleteOrder() {
    this.props.delete(this.props.order.id);
  }
}

export default Order;