import React, {Component} from 'react';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faPlusCircle} from '@fortawesome/free-solid-svg-icons'

import "../styles/commodity.less"

class Commodity extends Component {
  render() {
    const commodity = this.props.commodity;
    return (
      <div className={'Commodity'}>
        <img src={commodity.imageUrl} alt={'商品图片'}/>
        <h3>{commodity.name}</h3>
        <p>单价：{commodity.eachPrice} 元/ {commodity.measureWord}</p>
        <span onClick={this.addOrder.bind(this)} >
          <FontAwesomeIcon icon={faPlusCircle} className={'plusCircle'} />
        </span>
      </div>
    );
  }

  addOrder() {
    this.props.add(this.props.commodity.id);
  }
}

export default Commodity;