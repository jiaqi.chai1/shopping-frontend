export const getData = (url) => {
  return fetch(url).then(response => response.json());
};

export const postData = (url, data) => {
  return fetch(url, {
    method: 'POST',
    headers: {
      'Content-Type': 'application/json'
    },
    body: JSON.stringify(data)
  })
};

export const deleteData = (url, id) => {
  return fetch(url + "/" + id, {
    method: "DELETE"
  });
};