import {deleteData, getData} from "../utils/dataInteraction";
import {orderURL} from "../utils/consts";

export const getOrders = () => (dispatch) => {
  getData(orderURL)
    .then(result => {
        dispatch({
          type: 'GET_ORDERS',
          orders: result
        });
      }
    ).catch(() => {
    console.log("Get Orders Error");
  })
};

export const deleteOrder = (id, push) => (dispatch) => {
  deleteData(orderURL, id)
    .then(() => {
      dispatch({
        type: 'DELETE_ORDER'
      });
      // push("/commodity-orders")
      window.location.reload();
      getOrders();
    }).catch(() => {
      console.log("Delete Order Error")
  })
};