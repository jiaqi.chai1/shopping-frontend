import {postData} from '../utils/dataInteraction';
import {commodityURL} from "../utils/consts";

export const addCommodity = (push) => (dispatch, getState) => {
  const states = getState().adding;
  const data = {name: states.name, eachPrice: states.eachPrice, measureWord: states.measureWord, imageUrl: states.imageUrl};

  postData(commodityURL, data).then(() => {
    dispatch({
      type: "ADD_COMMODITY"
    });
    push("/");
  }).catch(() => console.log("Error while addingCommodity"))
};

export const stashName = (name) => (dispatch) => {
  dispatch({
    type: "STASH_NAME",
    name: name
  })
};

export const stashEachPrice = (eachPrice) => (dispatch) => {
  dispatch({
    type: "STASH_EACH_PRICE",
    eachPrice: eachPrice
  })
};

export const stashMeasureWord = (measureWord) => (dispatch) => {
  dispatch({
    type: "STASH_MEASURE_WORD",
    measureWord: measureWord
  })
};

export const stashImageUrl = (imageUrl) => (dispatch) => {
  dispatch({
    type: "STASH_IMAGE_URL",
    imageUrl: imageUrl
  })
};