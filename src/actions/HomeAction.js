import {getData, postData} from '../utils/dataInteraction';
import {commodityURL, orderURL} from "../utils/consts";

export const getCommodities = () => (dispatch) => {
  getData(commodityURL)
    .then(result => {
        dispatch({
          type: 'GET_COMMODITIES',
          commodities: result
        });
      }
    ).catch(() => {
    console.log("Get Commodities Error");
  })
};

export const addOrderQuantity = (id) => () => {
  const data = {commodityId: id};
  postData(orderURL, data)
    .then().catch(() => {
      console.log("Post Order Error")
  })
};