import React, {Component} from 'react';

import "../styles/commodityAdding.less"
import {connect} from "react-redux";
import {
  addCommodity,
  stashEachPrice,
  stashImageUrl,
  stashMeasureWord,
  stashName
} from "../actions/CommodityAddingAction";
import {bindActionCreators} from "redux";

class CommodityAdding extends Component {

  constructor(props) {
    super(props);

  }

  render() {
    return (
      <div className={'CommodityAdding'}>
        <h2>添加商品</h2>
        <p>名称: </p>
        <input placeholder={"名称"} onChange={this.stashName.bind(this)}/>
        <p>价格: </p>
        <input placeholder={"价格"} onChange={this.stashEachPrice.bind(this)}/>
        <p>单位: </p>
        <input placeholder={"单位"} onChange={this.stashMeasureWord.bind(this)}/>
        <p>图片: </p>
        <input placeholder={"URL"} onChange={this.stashImageUrl.bind(this)}/>
        <button onClick={this.addCommodity.bind(this)}>提交</button>
      </div>
    );
  }

  stashName(event) {
    this.props.stashName(event.target.value);
  }

  stashEachPrice(event) {
    this.props.stashEachPrice(event.target.value);
  }

  stashMeasureWord(event) {
    this.props.stashMeasureWord(event.target.value);
  }

  stashImageUrl(event) {
    this.props.stashImageUrl(event.target.value);
  }

  addCommodity() {
    this.props.addCommodity(this.props.history.push)
  }
}

const mapStateToProps = state => ({
  name: state.adding.name,
  eachPrice: state.adding.eachPrice,
  measureWord: state.adding.measureWord,
  imageUrl: state.adding.imageUrl
});

const mapDispatchToProps = dispatch => bindActionCreators({
  stashName,
  stashEachPrice,
  stashMeasureWord,
  stashImageUrl,
  addCommodity
}, dispatch);

export default connect(mapStateToProps, mapDispatchToProps)(CommodityAdding);