import React, {Component} from 'react';
import {connect} from "react-redux";
import {deleteOrder, getOrders} from "../actions/CommodityOrdersAction";
import {bindActionCreators} from "redux";
import Order from "../components/Order";

import "../styles/commodityOrders.less";

class CommodityOrders extends Component {

  componentDidMount() {
    this.props.getOrders();
  }

  render() {
    return (
      <div className={'CommodityOrders'}>
        <span>名字</span>
        <span>单价</span>
        <span>数量</span>
        <span>单位</span>
        <span>操作</span>
        <hr/>
        {this.props.orders.map((order, key) =>
          <Order key={key} order={order} delete={this.deleteOrder.bind(this)}/>
        )}
      </div>
    );
  }

  deleteOrder(id) {
    this.props.deleteOrder(id, this.props.history.push);
  }
}

const mapStateToProps = state => ({
  orders: state.order.orders
});

const mapDispatchToProps = dispatch => bindActionCreators({
  getOrders,
  deleteOrder
}, dispatch);

export default connect(mapStateToProps, mapDispatchToProps)(CommodityOrders);