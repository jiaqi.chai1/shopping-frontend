import React, {Component} from 'react';
import {addOrderQuantity, getCommodities} from "../actions/HomeAction";
import {connect} from "react-redux";
import {bindActionCreators} from "redux";
import Commodity from "../components/Commodity";

import '../styles/home.less';

class Home extends Component {

  componentDidMount() {
    this.props.getCommodities();
  }

  render() {
    return (
      <div className={'Home'}>
        {this.props.commodities.map((commodity, key) =>
          <Commodity key={key} commodity={commodity} add={this.addOrderQuantity.bind(this)}/>
        )}
      </div>
    );
  }

  addOrderQuantity(id) {
    this.props.addOrderQuantity(id);
  }
}

const mapStateToProps = state => ({
  commodities: state.home.commodities
});

const mapDispatchToProps = dispatch => bindActionCreators({
  getCommodities,
  addOrderQuantity
}, dispatch);

export default connect(mapStateToProps, mapDispatchToProps)(Home);