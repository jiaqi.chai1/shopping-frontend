import React, {Component} from 'react';
import {BrowserRouter as Router, NavLink} from "react-router-dom";
import {Route, Switch} from "react-router";
import Home from "./page/Home";
import CommodityAdding from "./page/CommodityAdding";
import {FontAwesomeIcon} from '@fortawesome/react-fontawesome'
import {faIgloo} from '@fortawesome/free-solid-svg-icons'

import './styles/App.less';
import CommodityOrders from "./page/CommodityOrders";

class App extends Component {
  render() {
    return (
      <div className="App">
        <Router>
          <header>
            <NavLink exact to={"/"} ><FontAwesomeIcon icon={faIgloo} className={'Igloo'}/>&nbsp;商城</NavLink>
            <NavLink exact to={"/commodity-orders"} >订单</NavLink>
            <NavLink exact to={"/commodity-adding"} >添加商品</NavLink>
          </header>
          <Switch>
            <Route path="/commodity-orders" component={CommodityOrders}/>
            <Route path="/commodity-adding" component={CommodityAdding}/>
            <Route path="/" component={Home}/>
          </Switch>
        </Router>
      </div>
    );
  };
}

export default App;